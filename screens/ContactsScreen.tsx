import React, {useEffect, useState} from 'react';
import { FlatList, StyleSheet } from 'react-native';
import { View } from '../components/Themed';
import ContactListItem from '../components/ContactListItem/ContactListItem';

//import users from '../data/Users';
import { API, graphqlOperation } from 'aws-amplify';
import { listUsers } from '../graphql/queries';

export default function ContactsScreen() {

  const [users, setUsers] = useState('');

  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const usersData = await API.graphql(
          graphqlOperation(
            listUsers,
          )
        )
        setUsers(usersData.data.listUsers.items);
      } catch (e) {

      }
    }
    fetchUsers();
  }, []);

  return (
    <View style={styles.container}>
      <FlatList
        data={users}
        renderItem={({ item }) => <ContactListItem user={item} />}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  }
});
