import React, { useEffect, useState } from 'react';
import { TextInput, View, KeyboardAvoidingView, Platform, Keyboard } from 'react-native';
import { 
    Entypo, 
    FontAwesome5, 
    Fontisto, 
    MaterialCommunityIcons, 
    MaterialIcons
} from '@expo/vector-icons';
import styles from './styles';
import { TouchableOpacity } from 'react-native-gesture-handler';

import {
    API,
    Auth,
    graphqlOperation
} from 'aws-amplify';
import { 
    createMessage, 
    updateChatRoom, 
} from '../../graphql/mutations';

const InputBox = (props) => {
    const { chatRoomID } = props;

    const [message, setMessage] = useState('');
    const [myUserId, setMyUserId] = useState(null);

    useEffect(() => {
        const fetchUser = async () => {
            const userInfo = await Auth.currentAuthenticatedUser();
            setMyUserId(userInfo.attributes.sub);
        }
        fetchUser();
    }, []);

    const onMicrophone = () => {
        console.warn('Microphone');
    }

    const updateChatRoomLastMessage = async (messageID: string) => {
        try {
            await API.graphql(
                graphqlOperation(
                    updateChatRoom, {
                        input: {
                            id: chatRoomID,
                            lastMessageID: messageID
                        }
                    }
                )
            )
        } catch (e) {
            console.log(e);
        }
    }

    const onSendPress = async () => {
        // send to backend
        try {
            const newMessagesData = await API.graphql(
                graphqlOperation(
                    createMessage, 
                    {
                        input: {
                            content: message,
                            userID: myUserId,
                            chatRoomID: chatRoomID
                        }
                    }
                )
            )
            await updateChatRoomLastMessage(newMessagesData.data.createMessage.id);
            Keyboard.dismiss()
        } catch(e) {
            console.log(e);
        }
        setMessage('');
        Keyboard.dismiss()
    }

    const onPress = () => {
        if(!message) {
            onMicrophone();
        } else {
            onSendPress();
        }
    }

    return (
        <KeyboardAvoidingView
            behavior={Platform.OS === "ios" ? "padding" : "height"}
            keyboardVerticalOffset={Platform.OS === "ios" ? 100 : 0}
            style={styles.container}
        >
            <View style={styles.mainContainer}>
                <FontAwesome5 name="laugh-beam" size={24} color="grey" />
                <TextInput 
                    style={styles.textInput} 
                    placeholder="Type a message"
                    multiline
                    numberOfLines={3}
                    maxHeight={60}
                    value={message}
                    onChangeText={setMessage}
                />
                <Entypo name="attachment" size={24} color="grey" style={styles.icon} />
                {!message && <Fontisto name="camera" size={24} color="grey" style={styles.iconCamera} />}
            </View>
            <TouchableOpacity
                activeOpacity={0.8} 
                onPress={onPress}
            >
                <View style={styles.buttonContainer}>
                    {
                        !message ? (
                            <MaterialCommunityIcons name="microphone" size={24} color="white" />
                        ) :
                        (
                            <MaterialIcons name="send" size={24} color="white" />
                        )
                    }
                </View>
            </TouchableOpacity>
        </KeyboardAvoidingView>
    )
}

export default InputBox;