import moment from 'moment';
import React from 'react';
import { Text, View, Image } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { User } from '../../types';
import styles from './style';

import {
    API,
    graphqlOperation,
    Auth
} from 'aws-amplify';

import {
    createChatRoom,
    createChatRoomUser
} from '../../graphql/mutations';

export type ContactListItemProps = {
    user: User
}

const ContactListItem = ({ user }: ContactListItemProps) => {

    const navigation = useNavigation();

    const onClick = async () => {
        try {
            // Create a new Chat Room
            const newChatRoomData = await API.graphql(graphqlOperation(createChatRoom, {
                input: {
                    lastMessageID: "112d2514-f4c9-4def-8fc5-d4d87f28490c"
                }
            }));

            // Add `User` to the Chat Room
            if(!newChatRoomData.data) {
                console.log("Failed to create a chat room");
                return;
            }

            const newChatRoom = newChatRoomData.data.createChatRoom;

            await API.graphql(
                graphqlOperation(
                    createChatRoomUser, {
                        input: {
                            userID: user.id,
                            chatRoomID: newChatRoom.id
                        }
                    }
                )
            );

            // Add authenticated user to the Chat Room
            const userInfo = await Auth.currentAuthenticatedUser();
            await API.graphql(
                graphqlOperation(
                    createChatRoomUser, {
                        input: {
                            userID: userInfo.attributes.sub,
                            chatRoomID: newChatRoom.id
                        }
                    }
                )
            );

            // navigate

            navigation.navigate('ChatRoom', {
                id: newChatRoom.id,
                name: "Hard coded name",
            });

        } catch(e) {
            console.log(e);
        }
    }

    return (
        <TouchableWithoutFeedback onPress={onClick}>
            <View style={styles.container}>
                <View style={styles.leftContainer}>
                    <Image source={{ uri: user.imageUri || '' }} style={styles.avatar} />
                    <View style={styles.subContainer}>
                        <View style={styles.midContainer}>
                            <Text style={styles.username}>{ user.name }</Text> 
                        </View>
                        <View style={styles.content}>
                            <Text numberOfLines={1} style={styles.status}>{user.status}</Text>
                        </View>
                    </View>
                </View>
            </View>
        </TouchableWithoutFeedback>
    )
}

export default ContactListItem;