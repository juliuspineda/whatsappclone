import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
        padding: 10,
        // borderBottomWidth: 1,
        // borderBottomColor: '#cdcdcd'
    },
    leftContainer: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between'
    },
    subContainer: {
        flexDirection: 'column',
        width: '100%',
        justifyContent: 'space-evenly'
    },
    midContainer: {
        flexDirection: 'row',
        width: '78%',
        justifyContent: 'space-between',
    },
    content: {
        width: '70%'
    },
    avatar: {
        width: 60,
        height: 60,
        marginRight: 15,
        borderRadius: 50
    },
    username: {
        fontWeight: 'bold',
        fontSize: 16
    },
    status: {
        fontSize: 16,
        color: 'grey'
    },
    videoIcon: {
        marginRight: 10
    }
});

export default styles;